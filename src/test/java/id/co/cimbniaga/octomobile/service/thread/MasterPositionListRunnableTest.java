package id.co.cimbniaga.octomobile.service.thread;

import com.github.fppt.jedismock.RedisServer;
import id.co.cimbniaga.octomobile.Application;
import id.co.cimbniaga.octomobile.project.domain.dao.Position;
import id.co.cimbniaga.octomobile.project.repository.PositionRepository;
import id.co.cimbniaga.octomobile.project.service.cache.CacheManagerService;
import id.co.cimbniaga.octomobile.project.service.thread.MasterPositionListRunnable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class MasterPositionListRunnableTest {

    @MockBean
    MasterPositionListRunnable masterPositionListRunnable;

    @Mock
    PositionRepository positionRepository;

    @Mock
    CacheManagerService cacheManagerService;

    @Mock
    Page page;

    @Test
    public void run_empty() {
        Mockito.when(positionRepository.findAll((Pageable) Mockito.any())).thenReturn(page);
        Mockito.when(page.getContent()).thenReturn(new ArrayList<Position>());
        masterPositionListRunnable.run();
        Assertions.assertTrue(true);
    }

    @Test
    public void run_noEmpty() {
        Position position = Position.builder().postionCode("POS5").build();
        List<Position> positionList = new ArrayList<>();
        positionList.add(position);

        Mockito.when(positionRepository.findAll((Pageable) Mockito.any())).thenReturn(page);
        Mockito.when(page.getContent()).thenReturn(positionList);
        masterPositionListRunnable.run();
        Assertions.assertTrue(true);
    }
}
