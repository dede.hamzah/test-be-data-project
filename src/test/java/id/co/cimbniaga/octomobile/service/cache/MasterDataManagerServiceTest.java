package id.co.cimbniaga.octomobile.service.cache;

import com.github.fppt.jedismock.RedisServer;
import id.co.cimbniaga.octomobile.Application;
import id.co.cimbniaga.octomobile.project.constant.Constant;
import id.co.cimbniaga.octomobile.project.service.cache.MasterDataManagerService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class MasterDataManagerServiceTest {

    @Autowired
    MasterDataManagerService masterDataManagerService;

    private RedisServer redisServer;

    @BeforeEach
    public void doBefore() throws IOException {
        if (redisServer != null) {
            redisServer.stop();
        }

        redisServer = RedisServer.newRedisServer(16379);
        redisServer.start();
    }

    @Test
    public void reload_ALL(){
        masterDataManagerService.reload(Constant.MasterDataType.ALL);
        Assertions.assertTrue(true);
    }

    @Test
    public void reload_Position(){
        masterDataManagerService.reload(Constant.MasterDataType.POSITION_LIST);
        Assertions.assertTrue(true);
    }
}
