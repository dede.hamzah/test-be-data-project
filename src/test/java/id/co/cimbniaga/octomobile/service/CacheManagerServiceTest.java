package id.co.cimbniaga.octomobile.service;

import com.github.fppt.jedismock.RedisServer;
import id.co.cimbniaga.octomobile.Application;
import id.co.cimbniaga.octomobile.project.domain.dao.Position;
import id.co.cimbniaga.octomobile.project.service.cache.CacheManagerService;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class CacheManagerServiceTest {

    @Autowired
    private CacheManagerService cacheManagerService;

    private RedisServer redisServer;

    @Before
    public void doBefore() throws IOException {
        if (redisServer != null) {
            redisServer.stop();
        }

        redisServer = RedisServer.newRedisServer(16379);
        redisServer.start();
    }

    @After
    public void afterTest(){
        redisServer.stop();
    }

    @Test
    public void putCacheTest(){
        cacheManagerService.putCache("CACHE_NAME", "key", "data", 60);
        assertEquals("data", (String) cacheManagerService.getCache("CACHE_NAME", "key"));
    }

    @Test
    public void getCacheTest(){
        cacheManagerService.putCache("CACHE_NAME", "key", "data", 60);
        assertEquals("data", (String) cacheManagerService.getCache("CACHE_NAME", "key"));
    }

    @Test
    public void removeCacheTest(){
        cacheManagerService.putCache("CACHE_NAME", "key", "data", 60);
        cacheManagerService.removeCache("CACHE_NAME", "key");
        assertNull(cacheManagerService.getCache("CACHE_NAME", "key"));
    }

    @Test
    public void putAllToCacheTestd(){
        Map<String, Object> data = new HashMap<>();
        data.put("key1", new Position());
        cacheManagerService.putAllToCache("CACHE_NAME", data);
        assertNotNull(cacheManagerService.getCache("CACHE_NAME", "key1"));
    }
}
