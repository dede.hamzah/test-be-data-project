package id.co.cimbniaga.octomobile.project.domain.dao;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Project implements Serializable {

    private static final long serialVersionUID = -6955588056453768329L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NonNull
    private long id;

    @Column(length = 20)
    private String projectCode;
    private String projectDescription;

    @Column(length = 10)
    private long mandays;

    @ManyToMany(mappedBy = "memberProject")
    private List<Employee> memberEmployee = new ArrayList<>();

    @OneToOne()
    @JoinTable(name = "lead_project",
            joinColumns = @JoinColumn(name = "project_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "id"))
    private Employee leadEmployee;
}
