package id.co.cimbniaga.octomobile.project.domain.dao;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Position implements Serializable {

    private static final long serialVersionUID = 4893613897246823573L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NonNull
    private long id;

    @Column(length = 20)
    private String postionCode;
    private String positionName;
    private String positionDescription;
}
