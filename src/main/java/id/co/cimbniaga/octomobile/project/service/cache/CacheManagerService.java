package id.co.cimbniaga.octomobile.project.service.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service(value = "CacheManagerServiceParameter")
@Slf4j
public class CacheManagerService {

    @Autowired
    @Qualifier("redisTemplateParameter")
    private RedisTemplate<String, Object> redisTemplate;

    public void putCache(String cacheName, Object key, Object value, long timeToLive){
        redisTemplate.opsForValue().set(cacheName + key, value, timeToLive, TimeUnit.SECONDS);
    }

    public void putAllToCache(String cacheName, Map<String, Object> data){
        Map<String, Object> dataToRedis = data
                .entrySet()
                .stream()
                .collect(
                        Collectors
                                .toMap(
                                        e -> cacheName.concat(e.getKey()),
                                        e -> e.getValue()
                                )
                );
        redisTemplate.opsForValue().multiSet(dataToRedis);
    }

    public Object getCache(String cacheName, Object key){
        return redisTemplate.opsForValue().get(cacheName + key);
    }

    public void removeCache (String cacheName, Object key){
        redisTemplate.opsForValue().getOperations().delete(cacheName + key);
    }
}
