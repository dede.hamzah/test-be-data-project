package id.co.cimbniaga.octomobile.project.domain.dao;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Employee implements Serializable {
    private static final long serialVersionUID = 6020048990700947705L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NonNull
    private long id;

    @Column(length = 20)
    private String nik;
    private String firstName;
    private String lastName;

    @Column(length = 15)
    private String gender;

    @Column(length = 15)
    private long salary;

    @Column(length = 30)
    private String title;

    @ManyToMany()
    @JoinTable(name = "member_project",
            joinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "project_id", referencedColumnName = "id"))
    private List<Project> memberProject = new ArrayList<>();

    @OneToMany(mappedBy = "leadEmployee")
    private List<Project> leadProject = new ArrayList<>();
}
