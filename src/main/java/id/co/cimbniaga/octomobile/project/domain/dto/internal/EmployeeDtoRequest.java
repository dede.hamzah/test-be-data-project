package id.co.cimbniaga.octomobile.project.domain.dto.internal;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import id.co.cimbniaga.octomobile.project.domain.dao.Project;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class EmployeeDtoRequest {
    private Long id;
    private String nik;
    private String firstName;
    private String lastName;
    private String gender;
    private Long salary;
    private String title;
    private List<Project> memberProject = new ArrayList<>();
}
