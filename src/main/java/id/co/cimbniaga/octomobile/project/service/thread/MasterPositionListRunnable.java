package id.co.cimbniaga.octomobile.project.service.thread;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.cimbniaga.octomobile.project.constant.Constant;
import id.co.cimbniaga.octomobile.project.domain.dao.Position;
import id.co.cimbniaga.octomobile.project.repository.PositionRepository;
import id.co.cimbniaga.octomobile.project.service.cache.CacheManagerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class MasterPositionListRunnable implements Runnable{
    private ObjectMapper objectMapper = new ObjectMapper();
    private int chunk;
    private CacheManagerService cacheManagerService;
    private PositionRepository positionRepository;

    private static final String SORT_PROPERTY = "id";

    public MasterPositionListRunnable(PositionRepository positionRepository, int chunk, CacheManagerService cacheManagerService) {
        this.chunk = chunk;
        this.cacheManagerService = cacheManagerService;
        this.positionRepository = positionRepository;
    }

    @Override
    public void run() {
        log.info("Processing to up master data {} to cache", Constant.CacheName.POSITION_LIST);

        int index = 0;

        Pageable pageable = PageRequest.of(index, chunk, Sort.Direction.ASC, SORT_PROPERTY);
        List<Position> positionList = positionRepository.findAll(pageable).getContent();
        Map<String, Object> data = new HashMap<>();

        while (!positionList.isEmpty()) {
            for (Position position : positionList) {
                data.put(position.getPostionCode(),
                        objectMapper.convertValue(position, new TypeReference<Map<String, Object>>() {}));
            }

            index++;
            pageable = PageRequest.of(index, chunk, Sort.Direction.ASC, SORT_PROPERTY);
            positionList = positionRepository.findAll(pageable).getContent();
        }

        if (!data.isEmpty()) {
            cacheManagerService.putAllToCache(Constant.CacheName.POSITION_LIST, data);
        }

        log.info("up {} data of {} to cache done!!!", data.keySet().size(), Constant.CacheName.POSITION_LIST);
    }
}
