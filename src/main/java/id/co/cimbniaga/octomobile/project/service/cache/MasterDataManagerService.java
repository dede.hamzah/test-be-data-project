package id.co.cimbniaga.octomobile.project.service.cache;

import id.co.cimbniaga.octomobile.project.constant.Constant;
import id.co.cimbniaga.octomobile.project.repository.PositionRepository;
import id.co.cimbniaga.octomobile.project.service.thread.MasterPositionListRunnable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MasterDataManagerService {

    @Autowired
    private PositionRepository positionRepository;

    @Autowired
    @Qualifier("CacheManagerServiceParameter")
    private  CacheManagerService cacheManagerService;

    @Value("${master.data.thread.pool.prefix:MASTERDATA-THREAD-}")
    String prefix;

    @Value("${master.data.thread.pool.core.size:10}")
    int corePoolSize;

    @Value("${master.data.thread.pool.max.size:100}")
    int maxPoolSize;

    @Value("${master.data.thread.pool.chunk:100}")
    int chunk;

    @Value("${master.data.thread.pool.waitOnShutdown:true}")
    boolean waitForTasksToCompleteOnShutdown;

    @EventListener(ApplicationReadyEvent.class)
    private void onApplicationReady() {
        log.info("Start thread ApplicationReadyEvent");
        reload(Constant.MasterDataType.ALL);
    }

    public void reload(String masterDataType) {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(corePoolSize);
        threadPoolTaskExecutor.setMaxPoolSize(maxPoolSize);
        threadPoolTaskExecutor.setThreadNamePrefix("Executor-".concat(prefix));
        threadPoolTaskExecutor.setAwaitTerminationSeconds(Constant.INT_ONE_DAY_IN_SECOND);
        threadPoolTaskExecutor.setWaitForTasksToCompleteOnShutdown(waitForTasksToCompleteOnShutdown);
        threadPoolTaskExecutor.initialize();

        switch (masterDataType) {
            case Constant.MasterDataType.POSITION_LIST:
                threadPoolTaskExecutor.execute(new MasterPositionListRunnable(positionRepository, chunk, cacheManagerService));
                break;
            case Constant.MasterDataType.ALL:
                reloadAll(threadPoolTaskExecutor);
                break;
            default:
                break;

        }
    }

    private void reloadAll(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        log.info("Execute All");
        threadPoolTaskExecutor.execute(new MasterPositionListRunnable(positionRepository, chunk, cacheManagerService));
    }
}
