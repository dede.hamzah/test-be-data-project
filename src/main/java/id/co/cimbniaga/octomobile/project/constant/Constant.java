package id.co.cimbniaga.octomobile.project.constant;

public class Constant {
    private Constant() {}

    public static class ResponseKey{
        private ResponseKey(){}
        public static final String KEY_SUCCESS = "SUCCESS";
        public static final String KEY_SUCCESS_INSERT = "SUCCESS INSERT DATA";
        public static final String KEY_SUCCESS_UPDATE = "SUCCESS UPDATE DATA";
        public static final String KEY_SUCCESS_DELETE = "SUCCESS DELETE DATA";
        public static final String KEY_INVALID_REQUEST = "INVALID_REQUEST";
        public static final String KEY_DATA_NOT_FOUND = "DATA_NOT_FOUND";
    }

    public static class MasterDataType{
        private MasterDataType(){}
        public static final String POSITION_LIST = "POSITION_LIST";
        public static final String ALL = "ALL";
    }

    public static class CacheName{
        private CacheName(){}
        public static final String POSITION_LIST = "POSITION_LIST";
    }

    public static final Integer INT_ONE_DAY_IN_SECOND = (60 * 60 * 24);
}
