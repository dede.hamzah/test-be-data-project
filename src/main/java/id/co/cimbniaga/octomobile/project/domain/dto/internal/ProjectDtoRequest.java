package id.co.cimbniaga.octomobile.project.domain.dto.internal;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import id.co.cimbniaga.octomobile.project.domain.dao.Employee;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ProjectDtoRequest {
    private Long id;
    private String projectCode;
    private String projectDescription;
    private long mandays;
    private Employee leadEmployee;
}
