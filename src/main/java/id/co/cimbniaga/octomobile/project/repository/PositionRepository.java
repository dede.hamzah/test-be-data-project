package id.co.cimbniaga.octomobile.project.repository;

import id.co.cimbniaga.octomobile.project.domain.dao.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionRepository extends JpaRepository<Position, Long> {
}
