package id.co.cimbniaga.octomobile.project.controller;

import id.co.cimbniaga.octomobile.project.domain.dto.internal.ProjectDtoRequest;
import id.co.cimbniaga.octomobile.project.service.ProjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/project")
@Api(tags = "Project Data API")
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @PostMapping(value = "/")
    @ApiOperation(value = "Inquiry Project All Information")
    public ResponseEntity<Object> findAllProject() {
        try {
            return projectService.listProject();
        } catch (Exception e) {
            log.error("Happened error when findAll project : {}", e.getClass().getName(), e.getStackTrace());
            log.trace("{}", e);
            throw e;
        }
    }

    @PostMapping(value = "/detail")
    @ApiOperation(value = "Inquiry Project Information By Project Code")
    public ResponseEntity<Object> getProject(@ApiParam @RequestBody ProjectDtoRequest request) {
        try {
            return projectService.getProject(request.getProjectCode());
        } catch (Exception e) {
            log.error("Happened error when get project with projectCode: {}", e.getClass().getName(), e.getStackTrace());
            log.trace("{}", e);
            throw e;
        }
    }

    @PostMapping(value = "/create")
    @ApiOperation(value = "Create Project Data")
    public ResponseEntity<Object> insertProject(@ApiParam @RequestBody ProjectDtoRequest request) {
        try {
            return projectService.insertProject(request);
        } catch (Exception e) {
            log.error("Happened error when insert employee : {}", e.getClass().getName(), e.getStackTrace());
            log.trace("{}", e);
            throw e;
        }
    }

    @PostMapping(value = "/delete")
    @ApiOperation(value = "Delete Project Data")
    public ResponseEntity<Object> deleteProject(@ApiParam @RequestBody ProjectDtoRequest request) {
        try {
            return projectService.deleteProject(request.getId());
        } catch (Exception e) {
            log.error("Happened error when delete project : {}", e.getClass().getName(), e.getStackTrace());
            log.trace("{}", e);
            throw e;
        }
    }

    @PostMapping(value = "/update")
    @ApiOperation(value = "Update Project Data")
    public ResponseEntity<Object> updateEmployee(@ApiParam @RequestBody ProjectDtoRequest request) {
        try {
            return projectService.updateProject(request);
        } catch (Exception e) {
            log.error("Happened error when update project : {}", e.getClass().getName(), e.getStackTrace());
            log.trace("{}", e);
            throw e;
        }
    }
}
