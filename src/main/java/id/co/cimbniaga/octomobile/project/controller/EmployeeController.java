package id.co.cimbniaga.octomobile.project.controller;

import id.co.cimbniaga.octomobile.project.domain.dto.internal.EmployeeDtoRequest;
import id.co.cimbniaga.octomobile.project.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/employee")
@Api(tags = "Employee Data API")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping(value = "/detail")
    @ApiOperation(value = "Inquiry Employee Information By NIK")
    public ResponseEntity<Object> getEmployee(@ApiParam @RequestBody EmployeeDtoRequest request) {
        try {
            return employeeService.getEmployee(request.getNik());
        } catch (Exception e) {
            log.error("Happened error when get employee with NIK: {}", e.getClass().getName(), e.getStackTrace());
            log.trace("{}", e);
            throw e;
        }
    }

    @PostMapping(value = "/create")
    @ApiOperation(value = "Insert Employee Data")
    public ResponseEntity<Object> insertEmployee(@ApiParam @RequestBody EmployeeDtoRequest request) {
        try {
            return employeeService.insertEmployee(request);
        } catch (Exception e) {
            log.error("Happened error when insert employee : {}", e.getClass().getName(), e.getStackTrace());
            log.trace("{}", e);
            throw e;
        }
    }

    @PostMapping(value = "/update")
    @ApiOperation(value = "Update Employee Data")
    public ResponseEntity<Object> updateEmployee(@ApiParam @RequestBody EmployeeDtoRequest request) {
        try {
            return employeeService.updateEmployee(request);
        } catch (Exception e) {
            log.error("Happened error when update employee : {}", e.getClass().getName(), e.getStackTrace());
            log.trace("{}", e);
            throw e;
        }
    }

    @PostMapping(value = "/delete")
    @ApiOperation(value = "Delete Employee Data")
    public ResponseEntity<Object> deleteEmployee(@ApiParam @RequestBody EmployeeDtoRequest request) {
        try {
            return employeeService.deleteEmployee(request.getId());
        } catch (Exception e) {
            log.error("Happened error when delete employee : {}", e.getClass().getName(), e.getStackTrace());
            log.trace("{}", e);
            throw e;
        }
    }

    @PostMapping(value = "/")
    @ApiOperation(value = "Inquiry All Employee Information")
    public ResponseEntity<Object> findAllEmployee() {
        try {
            return employeeService.listEmployee();
        } catch (Exception e) {
            log.error("Happened error when findAll employee : {}", e.getClass().getName(), e.getStackTrace());
            log.trace("{}", e);
            throw e;
        }
    }
}
